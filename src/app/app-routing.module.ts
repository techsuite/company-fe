import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsPageComponent } from './pages/clients-page/clients-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { ServicesPageComponent } from './pages/services-page/services-page.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { TechnologiesPageComponent } from './pages/technologies-page/technologies-page.component';
import { WorkWithUsPageComponent } from './pages/work-with-us-page/work-with-us-page.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'services', component: ServicesPageComponent },
  { path: 'technologies', component: TechnologiesPageComponent },
  { path: 'clients', component: ClientsPageComponent },
  { path: 'team', component: TeamPageComponent },
  { path: 'work-with-us', component: WorkWithUsPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
