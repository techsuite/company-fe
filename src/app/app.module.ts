import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { ServicesPageComponent } from './pages/services-page/services-page.component';
import { TechnologiesPageComponent } from './pages/technologies-page/technologies-page.component';
import { ClientsPageComponent } from './pages/clients-page/clients-page.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { WorkWithUsPageComponent } from './pages/work-with-us-page/work-with-us-page.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { IconWithTitleComponent } from './components/icon-with-title/icon-with-title.component';
import { TeamMemberComponent } from './components/team-member/team-member.component';
import { SearchesResultsComponent } from './components/searches-results/searches-results.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatSliderModule } from '@angular/material/slider';
import { RequiredInputComponent } from './components/required-input/required-input.component';
import { RadioTableComponent } from './components/radio-table/radio-table.component';
import { SimplePopUpComponent } from './components/simple-pop-up/simple-pop-up.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    ServicesPageComponent,
    TechnologiesPageComponent,
    ClientsPageComponent,
    TeamPageComponent,
    WorkWithUsPageComponent,
    NavBarComponent,
    IconWithTitleComponent,
    TeamMemberComponent,
    SearchesResultsComponent,
    RequiredInputComponent,
    RadioTableComponent,
    SimplePopUpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatButtonModule,
    MatSliderModule,
    MatDialogModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
