import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon-with-title',
  templateUrl: './icon-with-title.component.html',
  styleUrls: ['./icon-with-title.component.scss'],
})
export class IconWithTitleComponent {
  @Input() iconSrc: string;
  @Input() title: string;
}
