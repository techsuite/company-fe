import { Component, EventEmitter, Input, Output } from '@angular/core';

export type RadioTableElement = { label: string; value: any };

@Component({
  selector: 'app-radio-table',
  templateUrl: './radio-table.component.html',
  styleUrls: ['./radio-table.component.scss'],
})
export class RadioTableComponent {
  @Input() options: string[];
  @Input() elements: RadioTableElement[];

  @Output() selection = new EventEmitter<{
    option: string;
    selected: any;
  }>();

  trackByItems(index) {
    return index;
  }

  getGridTemplateColumns() {
    return `5fr repeat(${this.options.length},1fr)`;
  }
}
