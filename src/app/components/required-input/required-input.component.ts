import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-required-input',
  templateUrl: './required-input.component.html',
  styleUrls: ['./required-input.component.scss'],
})
export class RequiredInputComponent {
  @Input() label: string;
  @Input() placeholder: string;

  @Input() value: string;
  @Output() valueChange = new EventEmitter<string>();

  updateValue(value: string) {
    this.value = value;
    this.valueChange.emit(value);
  }
}
