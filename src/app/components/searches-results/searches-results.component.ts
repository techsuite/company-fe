import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Search } from 'src/app/model/search';
import { SearchService } from 'src/app/service/search.service';

@Component({
  selector: 'app-searches-results',
  templateUrl: './searches-results.component.html',
  styleUrls: ['./searches-results.component.scss'],
})
export class SearchesResultsComponent implements OnInit {
  searches$: Observable<Search[]>;

  constructor(private searchService: SearchService) {}

  ngOnInit(): void {
    this.searches$ = this.searchService.getSearches();
  }

  openLink(search: string) {
    window.open(`http://www.google.com/search?q=${search}`);
  }
}
