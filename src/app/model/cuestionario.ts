export class Cuestionario {
  nombre: string;
  matricula: string;
  comentariosTecnologias: string;
  comentariosSoftskills: string;
  comentariosCargo: string;
  tecnologias: { id: number; gradoConocimiento: string }[];
  softskills: { id: number; rendimiento: string }[];
  cargos: { id: number; preferencia: string }[];

  constructor() {
    this.tecnologias = [];
    this.softskills = [];
    this.cargos = [];
  }
}
