export class Member {
  matricula: string;
  nombre: string;
  foto_url: string;
  categoria: 'executives' | 'developers';
}
