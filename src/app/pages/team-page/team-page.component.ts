import { Component, OnInit } from '@angular/core';
import { Member } from 'src/app/model/member';
import { MemberService } from 'src/app/service/member.service';

@Component({
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.scss'],
})
export class TeamPageComponent implements OnInit {
  executives: Member[];
  developers: Member[];

  constructor(private memberService: MemberService) {}

  ngOnInit(): void {
    this.memberService.getMembers().subscribe((response: Member[]) => {
      this.executives = response.filter((m) => m.categoria === 'executives');
      this.developers = response.filter((m) => m.categoria === 'developers');
    });
  }
}
