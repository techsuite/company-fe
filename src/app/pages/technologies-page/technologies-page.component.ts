import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  templateUrl: './technologies-page.component.html',
  styleUrls: ['./technologies-page.component.scss'],
})
export class TechnologiesPageComponent implements OnInit {
  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.apiService.getResponse('test').subscribe((resp) => {
      console.log(resp.status);
    });
  }
}
