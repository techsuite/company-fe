import { Component } from '@angular/core';
import { Cuestionario } from 'src/app/model/cuestionario';
import {
  CuestionarioParams,
  CuestionarioService,
} from 'src/app/service/cuestionario.service';
import { MatDialog } from '@angular/material/dialog';
import { SimplePopUpComponent } from 'src/app/components/simple-pop-up/simple-pop-up.component';

@Component({
  templateUrl: './work-with-us-page.component.html',
  styleUrls: ['./work-with-us-page.component.scss'],
})
export class WorkWithUsPageComponent {
  cuestionario: Cuestionario = new Cuestionario();
  params: CuestionarioParams;

  constructor(
    private cuestionarioService: CuestionarioService,
    private popUpService: MatDialog
  ) {}

  ngOnInit() {
    this.cuestionarioService
      .getParams()
      .subscribe((res) => (this.params = res));
  }

  isInvalid() {
    return !this.cuestionario.matricula || !this.cuestionario.nombre;
  }

  handleSelection(
    source: 'tecnologias' | 'softskills' | 'cargos',
    selectionId: number,
    option: string
  ) {
    let optionKey = '';
    switch (source) {
      case 'tecnologias':
        optionKey = 'gradoConocimiento';
        break;
      case 'softskills':
        optionKey = 'rendimiento';
        break;
      case 'cargos':
        optionKey = 'preferencia';
        break;
    }

    const currentElement = (this.cuestionario[source] as any).find(
      (e) => e.id === selectionId
    );

    if (!currentElement) {
      (this.cuestionario[source] as any).push({
        id: selectionId,
        [optionKey]: option,
      });
    } else {
      currentElement[optionKey] = option;
    }
  }

  submit() {
    this.cuestionarioService
      .newCuestionario(this.cuestionario)
      .subscribe(() => this.popUpService.open(SimplePopUpComponent));
  }
}
