import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  readonly ENDPOINT = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  get<T>(endpoint: string): Observable<T> {
    return this.http.get<T>(`${this.ENDPOINT}/${endpoint}`);
  }

  getResponse<T>(endpoint: string): Observable<HttpResponse<T>> {
    return this.http.get<T>(`${this.ENDPOINT}/${endpoint}`, {
      observe: 'response',
    });
  }

  post<T>(endpoint: string, body: any): Observable<T> {
    return this.http.post<T>(`${this.ENDPOINT}/${endpoint}`, body);
  }
}
