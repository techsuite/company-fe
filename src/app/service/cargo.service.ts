import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cargo } from '../model/cargo';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class CargoService {
  constructor(private api: ApiService) {}

  getCargos(): Observable<Cargo[]> {
    return this.api.get('cargo');
  }

  getOpcionesPreferencia(): Observable<string[]> {
    return this.api.get('cargo/opciones-preferencia');
  }
}
