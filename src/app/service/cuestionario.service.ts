import { Injectable } from '@angular/core';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { CargoService } from 'src/app/service/cargo.service';
import { SoftskillService } from 'src/app/service/softskill.service';
import { TecnologiaService } from 'src/app/service/tecnologia.service';
import { RadioTableElement } from '../components/radio-table/radio-table.component';
import { Cargo } from '../model/cargo';
import { Cuestionario } from '../model/cuestionario';
import { Softskill } from '../model/softskill';
import { Tecnologia } from '../model/tecnologia';
import { ApiService } from './api.service';

export type CuestionarioParams = {
  tecnologias: RadioTableElement[];
  cargos: RadioTableElement[];
  softskills: RadioTableElement[];
  opciones: {
    conocimiento: string[];
    rendimiento: string[];
    preferencia: string[];
  };
};

@Injectable({ providedIn: 'root' })
export class CuestionarioService {
  constructor(
    public api: ApiService,
    public tecnologiaService: TecnologiaService,
    public softskillService: SoftskillService,
    public cargoService: CargoService
  ) {}

  getParams(): Observable<CuestionarioParams> {
    return zip(
      this.tecnologiaService.getTecnologias(),
      this.cargoService.getCargos(),
      this.softskillService.getSoftskills(),
      this.tecnologiaService.getOpcionesGradoConocimiento(),
      this.softskillService.getOpcionesRendimiento(),
      this.cargoService.getOpcionesPreferencia()
    ).pipe(
      map(
        ([
          tecnologias,
          cargos,
          softskills,
          opcionesConocimiento,
          opcionesRendimiento,
          opcionesPreferencia,
        ]) => ({
          tecnologias: tecnologias.map(this.fieldToRadioTableElement),
          softskills: softskills.map(this.fieldToRadioTableElement),
          cargos: cargos.map(this.fieldToRadioTableElement),
          opciones: {
            conocimiento: opcionesConocimiento,
            rendimiento: opcionesRendimiento,
            preferencia: opcionesPreferencia,
          },
        })
      )
    );
  }

  private fieldToRadioTableElement(
    field: Tecnologia | Cargo | Softskill
  ): RadioTableElement {
    return {
      label: field.nombre,
      value: field.id,
    };
  }

  newCuestionario(cuestionario: Cuestionario): Observable<void> {
    return this.api.post('cuestionario', cuestionario);
  }
}
