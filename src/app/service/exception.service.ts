import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ExceptionService {
  constructor(private router: Router) {}

  handleHttpError(error: HttpErrorResponse) {
    switch (error.status) {
      case 404:
        console.log('error 404');

        this.router.navigate(['/']);
        break;
    }
  }
}
