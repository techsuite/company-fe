import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from '../model/member';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class MemberService {
  constructor(private api: ApiService) {}

  getMember(id: string): Observable<Member> {
    return this.api.get(`team/${id}`);
  }

  getMembers(): Observable<Member[]> {
    return this.api.get('team');
  }
}
