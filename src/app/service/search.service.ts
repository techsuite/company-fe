import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Search } from '../model/search';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class SearchService {
  constructor(private api: ApiService) {}

  getSearches(): Observable<Search[]> {
    return this.api.get('search');
  }
}
