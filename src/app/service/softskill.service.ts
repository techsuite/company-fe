import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Softskill } from '../model/softskill';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class SoftskillService {
  constructor(private api: ApiService) {}

  getSoftskills(): Observable<Softskill[]> {
    return this.api.get('softskill');
  }

  getOpcionesRendimiento(): Observable<string[]> {
    return this.api.get('softskill/opciones-rendimiento');
  }
}
