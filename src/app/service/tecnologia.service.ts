import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tecnologia } from '../model/tecnologia';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class TecnologiaService {
  constructor(private api: ApiService) {}

  getTecnologias(): Observable<Tecnologia[]> {
    return this.api.get('tecnologia');
  }

  getOpcionesGradoConocimiento(): Observable<string[]> {
    return this.api.get('tecnologia/opciones-conocimiento');
  }
}
